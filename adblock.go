package adblock

import (
	"context"
	"fmt"
	"io"
	"net"
	"os"

	"github.com/coredns/coredns/plugin"
	"github.com/coredns/coredns/plugin/metrics"
	clog "github.com/coredns/coredns/plugin/pkg/log"
	"github.com/coredns/coredns/request"

	"github.com/miekg/dns"
)

var log = clog.NewWithPlugin("adblock")

// comment
type Adblock struct {
	Next plugin.Handler
	*Blockfile
}

// comment
func (a Adblock) ServeDNS(ctx context.Context, w dns.ResponseWriter, r *dns.Msg) (int, error) {
	log.Info("Received response")

	state := request.Request{W: w, Req: r}
	qname := state.Name()

	log.Info(qname)

	block := false

	for _, i := range a.Blocks {
		if i == qname {
			block = true
		}
	}

	if block {
		//zone := plugin.Zones(a.Blocks).Matches(qname)
		//if zone == "" {
		// PTR zones don't need to be specified in Origins.
		//		if state.QType() != dns.TypePTR {
		//			// if this doesn't match we need to fall through regardless of h.Fallthrough
		//			return plugin.NextOrFailure(a.Name(), a.Next, ctx, w, r)
		//		}
		//	}

		answers := []dns.RR{}

		switch state.QType() {
		case dns.TypeA:
			ips := make([]net.IP, 1)
			ips[0] = []byte{0, 0, 0, 0}
			answers = a.a(qname, 300, ips)
		case dns.TypeAAAA:
			ips := make([]net.IP, 1)
			ips[0] = []byte{0, 0, 0, 0}
			answers = a.aaaa(qname, 300, ips)
		}

		m := new(dns.Msg)
		m.SetReply(r)
		m.Authoritative = true
		m.Answer = answers

		w.WriteMsg(m)
		return dns.RcodeSuccess, nil

	} else {
		pw := NewResponsePrinter(w)

		// Export metric with the server label set to the current server handling the request.
		requestCount.WithLabelValues(metrics.WithServer(ctx)).Inc()

		// Call next plugin (if any).
		return plugin.NextOrFailure(a.Name(), a.Next, ctx, pw, r)
	}
}

func (a Adblock) Name() string { return "adblock" }

// ResponsePrinter wrap a dns.ResponseWriter and will write example to standard output when WriteMsg is called.
type ResponsePrinter struct {
	dns.ResponseWriter
}

// NewResponsePrinter returns ResponseWriter.
func NewResponsePrinter(w dns.ResponseWriter) *ResponsePrinter {
	return &ResponsePrinter{ResponseWriter: w}
}

// WriteMsg calls the underlying ResponseWriter's WriteMsg method and prints "example" to standard output.
func (r *ResponsePrinter) WriteMsg(res *dns.Msg) error {
	fmt.Fprintln(out, "adblock")
	return r.ResponseWriter.WriteMsg(res)
}

// Make out a reference to os.Stdout so we can easily overwrite it for testing.
var out io.Writer = os.Stdout

// a takes a slice of net.IPs and returns a slice of A RRs.
func (a Adblock) a(zone string, ttl uint32, ips []net.IP) []dns.RR {
	answers := make([]dns.RR, len(ips))
	for i, ip := range ips {
		r := new(dns.A)
		r.Hdr = dns.RR_Header{Name: zone, Rrtype: dns.TypeA, Class: dns.ClassINET, Ttl: ttl}
		r.A = ip
		answers[i] = r
	}
	return answers
}

// aaaa takes a slice of net.IPs and returns a slice of AAAA RRs.
func (a Adblock) aaaa(zone string, ttl uint32, ips []net.IP) []dns.RR {
	answers := make([]dns.RR, len(ips))
	for i, ip := range ips {
		r := new(dns.AAAA)
		r.Hdr = dns.RR_Header{Name: zone, Rrtype: dns.TypeAAAA, Class: dns.ClassINET, Ttl: ttl}
		r.AAAA = ip
		answers[i] = r
	}
	return answers
}
