package adblock

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"sync"
	"time"
)

type Blockfile struct {
	sync.RWMutex
	Blocks []string
	path   string
	size   int64
	mtime  time.Time
}

func (b *Blockfile) parse(r io.Reader) {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := scanner.Bytes()
		if i := bytes.Index(line, []byte{'#'}); i >= 0 {
			// Discard comments.
			line = line[0:i]
		}
		f := bytes.Fields(line)
		if len(f) < 2 {
			continue
		}

		b.Blocks = append(b.Blocks, string(line))
	}

	return
}

func (b *Blockfile) readBlocks() {
	file, err := os.Open(b.path)
	if err != nil {
		// We already log a warning if the file doesn't exist or can't be opened on setup. No need to return the error here.
		return
	}
	defer file.Close()

	stat, err := file.Stat()
	b.RLock()
	size := b.size
	b.RUnlock()

	if err == nil && b.mtime.Equal(stat.ModTime()) && size == stat.Size() {
		return
	}

	b.parse(file)

	b.Lock()

	// Update the data cache.
	b.mtime = stat.ModTime()
	b.size = stat.Size()

	b.Unlock()
}
